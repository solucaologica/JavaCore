package br.com.stefanini.interfaces;

public interface Autenticavel {
	boolean validarSenha(int senhaDigitada);
}
