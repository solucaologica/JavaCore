package br.com.stefanini.interfaces;
import br.com.stefanini.poo.*;

public class AutenticacaoTeste {

	public static void main(String[] args) {
		
		int senhaDigitada = 123;
		
		Gerente g = new Gerente();
		Diretor d = new Diretor();
		Cliente c = new Cliente();
		
		Autenticavel gA = new Gerente();
		Autenticavel dA = new Diretor();
		Autenticavel cA = new Cliente();
		
		testeAutenticacao(senhaDigitada, cA);
				
	}
	
	public static void testeAutenticacao(int senha, Autenticavel aut) {
		if(aut.validarSenha(senha))
			System.out.println("Autenticado");
		else
			System.out.println("Erro");
	}
}
