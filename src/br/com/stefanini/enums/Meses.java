package br.com.stefanini.enums;

public enum Meses {
	JANEIRO(1, "Jan"), FEVEREIRO(2,"Fev"), MARCO(3, "Mar"), ABRIL(4, "Abr"), MAIO(5, "Mai"), 
	JUNHO(6, "Jun"), JULHO(7, "Jul"), AGOSTO(8, "AGO"), SETEMBRO(9, "Set"), OUTUBRO(10, "Out"),
	NOVEMBRO(11, "Nov"), DEZEMBRO(12, "Dez");
	
	private final int ordem;
	private final String abrev;
	
	Meses(int ordem, String abrev){
		this.ordem = ordem;
		this.abrev = abrev;
	}
	
	public boolean ultimoMesDoAno() {
		return this==DEZEMBRO;
	}
}
