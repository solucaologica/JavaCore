package br.com.stefanini.enums;
 /**
  * Javadocs Treinamento Stefanini
  * @author <a href="mailto:bruno@stefanini.com">Bruno A. P�dua</a>
  *	@param Teste nonoonono
  *	@return
  */

public class Teste {

	public static void main(String[] args) {
		Meses jan = Meses.JANEIRO;
		Meses dez = Meses.DEZEMBRO;
		
		oAnoNovo(dez);
	}
	
	private static void oAnoNovo(final Meses mes) {
		System.out.println(mes.ultimoMesDoAno() ? "SIM" : "N�O");
		System.out.println(mes.ordinal());
		System.out.println(mes.name());
	}
}
