package br.com.stefanini.matrizes;

public class Agenda {
	
	private String nome;
	private Contato[] contatos = new Contato[3];	
	
	
	public Contato[] getContatos() {
		return contatos;
	}

	public void setContatos(Contato[] contatos) {
		this.contatos = contatos;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
