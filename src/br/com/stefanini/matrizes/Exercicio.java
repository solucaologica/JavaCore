package br.com.stefanini.matrizes;

import java.util.Scanner;

public class Exercicio {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Informe o nome da agenda");
		String nome = sc.nextLine();
		
		Agenda agenda = new Agenda();
		agenda.setNome(nome);
		
		Contato[] contatos = new Contato[3];
		for(int i=0; i<contatos.length;i++) {
			System.out.println("Contato: " + i);
			Contato c = new Contato();
			
			System.out.println("Informe o nome: ");
			c.setNome(sc.nextLine());
			
			System.out.println("Informe o email: ");
			c.setEmail(sc.nextLine());
			
			System.out.println("Informe o tel: ");
			c.setTel(sc.nextLine());
			
			contatos[i] = c;
		}
		
		agenda.setContatos(contatos);
	}
}
