package br.com.stefanini.excessoes;

import java.sql.SQLClientInfoException;

public class Excessao {
	public static void main(String[] args) {
		/*
		 * MULTICATCH
		 */
		try {

		} catch (Exception e) {

		} catch (Throwable ex) {

		}
		/*
		 * JAVA 7
		 */
		try {

		} catch (ArrayIndexOutOfBoundsException | ArithmeticException e) {
		}
	}
}
