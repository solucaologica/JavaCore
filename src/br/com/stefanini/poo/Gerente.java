package br.com.stefanini.poo;

public class Gerente extends Funcionario{
	@Override
	public double getBonus() {
		return this.getSalario() * 0.5;
	}
}
