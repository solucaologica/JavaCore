package br.com.stefanini.poo;

public class Diretor extends Funcionario{
	@Override
	public double getBonus() {
		return this.getSalario() * 0.8;
	}
}
