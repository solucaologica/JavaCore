package br.com.stefanini.poo;
/*
 * Treinamento Stefanini Orientação a Objetos 2
 * @author Gustavo
 *
 */
public abstract class Funcionario {
	private float salario;
	
	
	public float getSalario() {
		return this.salario;
	}
	
	public void setSalario(float valor) {
		this.salario = valor;
	}
	
	public abstract double getBonus();
	
}
