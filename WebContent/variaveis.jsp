<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<title>Treinamento - Variaveis::</title>
</head>
<body>
	<!-- HEADER -->
	<%@include file="./inc/header.jsp"%>
	<!-- FIM HEADER -->
	<main> <!-- MENU --> <%@include file="./inc/menu.jsp"%>
	<!-- FIM MENU -->
	<div id="content">
		<h1>Variaveis</h1>
		<br />
		<p>
			Para armazenar valores no nosso c�digo, precisamos criar vari�veis,
			que s�o como caixas com nomes e tipos que armazenam valores de
			compat�veis com seus tipos. <br /> Temos duas op��es no momento que
			vamos declarar uma vari�vel, podemos declarar a vari�vel e j�
			inicializa-la com um valor ou podemos apenas declara-la e atribuir um
			valor posteriormente, vamos ver os dois cen�rios: <br />:
		</p>
		<br />
		<p>Para declarar uma variavel:</p>
		<br />
<textarea cols="60" rows="20">
public class Variaveis{
	public static void main(String[] args){
	int idade = 19;
	}
}
			</textarea>
		<br />
		<p>
			O <b>int</b> seria o tipo da nossa variavel, esse tipo em espec�fico
			� usado para armazenar n�meros inteiros. Vamos conhecer mais � frente
			outros tipos poss�veis como tipos para armazenar texto, n�meros
			decimais, datas e etc. <br /> <b>Idade</b> seria o nome da vari�vel
			que pode ser alterada para o nome que desejar. <br /> <b>19</b> � o
			valor que desejamos armazenar. <br />
		</p>
		<br />
<textarea cols="60" rows="20">
public class Variaveis2{
	public static void main(String[] args){
	int idade;
	idade = 19;
	}
}
</textarea>
<br />
		<p>
		Entre a declara��o e a inicializa��o voc� pode ter outras instru��es, desde que as instru��es n�o utilizem
		a vari�vel que ainda n�o est� inicializada, se o fizer receber� uma mensagem de erro.		
		</p>
<br/>
<textarea cols="60" rows="20">
public class Variaveis2{
	public static void main(Strin[]args){
	int a, b, c;//declarando
	int x = 1,  y = 2, z = 3;//declarando e inicializando 
	}
}
</textarea>
</div>
</body>
</html>
