<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<title>O que � e porque?::</title>
</head>
<body>
	<!-- HEADER -->
	<%@include file="./inc/header.jsp"%>
	<!-- FIM HEADER -->
	<main> <!-- MENU --> <%@include file="./inc/menu.jsp"%>
	<!-- FIM MENU -->
	<div id="content">
		<h1>O que �? E porque?</h1>
		<p>O java � free, voc� pode utiliza-lo tanto quanto em um ambiente
			de desenvolvimento, quanto em um ambiente de produ��o, sem precisar
			pagar nada por isso.</p>
		<p>Tamb�m podemos contar com editores/IDE's, servidores de
			aplica��o e bancos de dados totalmente gratuidos . Oque � muito bom
			para as empresas, deixando de gastar muito dinheiro com licen�as de
			software.</p>
		<br />
		<p>
			Linguagem de alto n�vel, ou seja, n�o se comunica diretamente com o
			c�digo de m�quina, necessita de um intermedi�rio para faz�-lo que no
			nosso caso � a <b>JVM - Java Virtual Machine</b>
		</p>
		<br />
		<p>Tudo come�a com a gente escrevendo um c�digo Java em um arquivo
			com extens�o '.java'(ex:HelloWorld.java) mas n�s n�o conseguimos
			executar esse arquivo diretamente, precisamos compilar o mesmo.</p>
		<br />
		<p>
			Para compilar utilizamos o <b>javac</b> apontando para o nosso
			arquivo .java com isso, vamos gerar um arquivo .class que no nosso
			exemplo seria HelloWorld.class, esses arquivos gerados ap�s a
			compila��o s�o conhecidos como <b>bytecodes</b>, uma forma
			intermediaria(pseudo-c�digo)entre o c�digo java e o c�digo de m�quina
		</p>
		<br />
		<p>
			E � ai que entra a JVM(Java Virtual Machine) que citamos anteriormente, ela � um programa
			que carrega e executa os aplicativos java, convertendo os bytecodes em c�digo execut�vel de m�quina.
		</p>
		<br/>
		<br/>
			HelloWorld.java ---> javac HelloWorld.java ---> HelloWorld.class(bytecode) ---> JVM ----> Executavel
		<br/>
		<br/>
		<p>
			Gra�as a JVM os programas s�o port�teis, rodam independentemente da plataforma, ou seja, rodam em
			qualquer sistema operacional sem a necessidade de reescrever o c�digo para adapt�-lo para um determinado,
			S.O. Se o sistema operacional tem uma JVM instalada ela rodar� seu c�digo sem problemas.
		</p>
		<br/>
		<p>
			O Java � uma linguagem de programa��o <b>orientada a objetos</b>, baseia-se em composi��es e itera��es
			entre diversas unidades de software que normalmente representam itens do mundo real, essas unidades s�o
			chamadas de objetos, o que nos acaba proporcionando um reuso muito grande do c�digo j� escrito e um c�digo
			de melhor qualidade.
		</p>
	</div>
	</main>
</body>
</html>