<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<title>Treinamento Auto Boxing - Unboxing::</title>
</head>
<body>
	<!-- HEADER -->
	<%@include file="./inc/header.jsp"%>
	<!-- FIM HEADER -->
	<main> 
	<!-- MENU -->
	<%@include file="./inc/menu.jsp"%>
	<!-- FIM MENU -->
	<div id="content">
		<h1>Treinamento AutoBoxing - Unboxing</h1>
		<br/>
		<p>
			Os processos de autoboxing e unboxing s�o efetuados implicitamente quando atribuimos um valor primitivo a uma
			variavel Wrapper(AutoBoxing).
			Quando atribu�mos uma instancia Wrapper a uma vari�vel do tipo primitivo realizamos o unboxing.
		</p>
		<br/>
		<%
		Byte b = 100; //byte para Byte
		Short s = 10000; // short para Short
		Integer i = 10000000; //int para Integer
		Long l = 100000000000L; //long para Long
		Character c = 'a'; //char para Character
		Boolean boo = true; //boolean para Boolean
		
		byte b2 = Byte.valueOf("100"); //Byte para byte
		short s2 = Short.valueOf("10000");// Short para short
		int i2 = Integer.valueOf("100000000"); //Integer para int
		long l2 = Long.valueOf("100000000000");//Long pra long
		char c2 = Character.valueOf('a');//Character para char
		boolean boo2 = Boolean.valueOf("true"); //Boolean para boolean
		
		%>
	</div>
	</main>
</body>
</html>