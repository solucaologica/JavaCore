
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<title>Wrappers::</title>
</head>
<body>
	<!-- HEADER -->
	<%@include file="./inc/header.jsp"%>
	<!-- FIM HEADER -->
	<main> 
	<!-- MENU -->
	<%@include file="./inc/menu.jsp"%>
	<!-- FIM MENU -->
	<div id="content">
		<h1>Wrappers</h1>
		<br/>
		<p>
			Wrappers s�o classes criadas para representar os tipos primitivos da linguagem java.
			<%
				int intPrimitivo = 10;
				Integer intWrapper = Integer.valueOf(intPrimitivo);  //int para Integer
				
				String intString = intWrapper.toString(); //Integer para String
				Integer intWrapper2 = Integer.valueOf(intString); //String para Integer
				
				byte bytePrimitivo = intWrapper.byteValue();//Integer para byte
				short shortPrimitivo = intWrapper.shortValue(); //Integer para short
				int intPrimitivo2 = intWrapper2.intValue(); //Integer para int
				long longPrimitivo = intWrapper2.longValue();//Integer para long
				float floatPrimitivo = intWrapper2.floatValue();// Integer para float
				double doublePrimitivo = intWrapper2.doubleValue();//Integer para double
				
				int intPrimitivo3 = Integer.parseInt(intString); //String para int
				
				out.println("\n MAX INTEGER: " + Integer.MAX_VALUE);
				out.println("\n MIN INTEGER: " + Integer.MIN_VALUE);
				
				boolean booleanPrimitivo = true;
				Boolean booleanWrapper = Boolean.valueOf(booleanPrimitivo);
				
				String	 booleanString = booleanWrapper.toString();//Boolean para String
				Boolean booleanWrapper2 = Boolean.valueOf(booleanString);//String para boolean
				boolean booleanPrimitivo2 = booleanWrapper2.booleanValue();//Boolean para boolean
				
				boolean booleanPrimitivo3 = Boolean.parseBoolean(booleanString);//String para boolean
				
				char charPrimitivo = 'a';
				Character charWrapper = Character.valueOf(charPrimitivo); //char para Character
				
				String charString = charWrapper.toString();//Character para String
				char charPrimitivo2 = charWrapper.charValue();//Character para char
				
			%>
		</p>
		
	</div>
	</main>
</body>
</html>