<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<title>Treinamento Tipos de Variáveis::</title>
</head>
<body>
	<!-- HEADER -->
	<%@include file="./inc/header.jsp"%>
	<!-- FIM HEADER -->
	<main> <!-- MENU --> <%@include file="./inc/menu.jsp"%>
	<!-- FIM MENU -->
	<div id="content">
		<h1>Tipos Variaveis</h1>
		<br />
		<h5>Tipos Primitivos</h5>
		<br />
		<p>Os tipos primitivos são os tipos mais básicos da linguagem:</p>
		<br />
		<h6>Numeros Inteiros</h6>
		<br />
		<table border="1">
			<tr>
				<td>TIPO</td>
				<td>DESCRIÇÃO</td>
				<td>DE</td>
				<td>ATÉ</td>
				<td>EXEMPLO:</td>
			</tr>
			<tr>
				<td>Byte</td>
				<td>O tipo byte é utilizado para armazenar números inteiros de
					8-bit</td>
				<td>-128</td>
				<td>127</td>
				<td>Nota de prova, dia do mês, mês do ano</td>
			</tr>
			<tr>
				<td>Short</td>
				<td>O tipo short é utilizado para armazenar números inteiros de
					16-bit</td>
				<td>-32768</td>
				<td>32767</td>
				<td>Ano</td>
			</tr>
			<tr>
				<td>Int</td>
				<td>O tipo int é utilizado para armazenar números inteiros de
					32-bit</td>
				<td>-2147483648</td>
				<td>2147483647</td>
				<td>Contador de visualizações</td>
			</tr>
			<tr>
				<td>Long</td>
				<td>O tipo long é utilizado para armazenar números inteiros de
					64-bit</td>
				<td>-9223372036854775808</td>
				<td>-9223372036854775807</td>
				<td>Número de protocolos</td>
			</tr>
		</table>
		<br />
		<h6>
			Decimais
			</h5>
			<br />
			<table border="1">
				<tr>
					<td>TIPO</td>
					<td>DESCRIÇÃO</td>
					<td>DE</td>
					<td>ATÉ</td>
					<td>EXEMPLO:</td>
				</tr>
				<tr>
					<td>Float</td>
					<td>O tipo float é utilizado para armazenar números decimais
						de 32-bit</td>
					<td>-3,4028 * 10³⁸</td>
					<td>3,4028 * 10³⁸</td>
					<td>Notas, medidas, peso</td>
				</tr>
				<tr>
					<td>Double</td>
					<td>O tipo double é utilizado para armazenar números decimais
						de 64-bit</td>
					<td>-1,7976E * 10³⁰⁸</td>
					<td>1,7976 * 10³⁰⁸</td>
					<td>Notas, medidas, peso</td>
				</tr>
			</table>
			<br />
			<h6>
				Outros tipos
				</h5>
				<br />
				<table border="1">
					<tr>
						<td>TIPO</td>
						<td>DESCRIÇÃO</td>
						<td>DE</td>
						<td>ATÉ</td>
						<td>EXEMPLO:</td>
					</tr>
					<tr>
						<td>Boolean</td>
						<td>O tipo boolean é utilizado para armazenar apenas 2
							valores true ou false</td>
						<td>0</td>
						<td>1</td>
						<td>Status, Confirmações</td>
					</tr>
					<tr>
						<td>Char</td>
						<td>O tipo char é utilizado para armazenar caracteres unicode
							de 16-bit</td>
						<td>\u0000</td>
						<td>\uffff</td>
						<td>Status em geral</td>
					</tr>
					<tr>
						<td>String</td>
						<td>O tipo string é utilizado para armazenar textos</td>
						<td>-</td>
						<td>-</td>
						<td>Status em geral</td>
					</tr>
				</table>
	</div>
	</main>
</body>
</html>
