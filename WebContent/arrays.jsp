<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<title>Treinamento Arrays::</title>
</head>
<body>
	<!-- HEADER -->
	<%@include file="./inc/header.jsp"%>
	<!-- FIM HEADER -->
	<main> 
	<!-- MENU -->
	<%@include file="./inc/menu.jsp"%>
	<!-- FIM MENU -->
	<div id="content">
		<h1>Arrays::</h1>
		<br/>
		<p>S�o utilizados quando desejamos agrupar um conjunto de valores.
		<br/>
		Podem ser unidimensionais, bidimensionais, tridimensionais etc....
		</p>
		<br/>
<textarea rows="10" cols="30">int[] notas = new int[3];</textarea>
		<br/>
		<p>
			Passar o tamanho do array � obrigat�rio e n�o poder� ser alterado, se necess�rio ser� preciso criar um novo array com o novo tamanho desejado.		
		</p>
		<br/>
		<br/>
		<h5>Array Bidimensional</h5>
		<br/>
		<textarea rows="10" cols="30">int[][]notas = new int[3][3]</textarea>
		<br/>
		<p>
			Acima declaramos e inicializamos um array bidimensional vazio do tipo int com tamanho <b>3x3</b>  sendo assim podemos
			<br/>
			adicionar 3 conjuntos com 3 valores em cada.
		</p>
		<br/>
		<p>
			Declarando e inicializando com valores:
		</p>
		<br/>
			<textarea rows="10" cols="60">int[]notas = new int[][]{{1,2,3},{4,5,6},{7,8,9}};</textarea>
		<br/>
		
	</div>
	</main>
</body>
</html>
