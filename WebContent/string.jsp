<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<title>Treinamento String::</title>
</head>
<body>
	<!-- HEADER -->
	<%@include file="./inc/header.jsp"%>
	<!-- FIM HEADER -->
	<main> <!-- MENU --> <%@include file="./inc/menu.jsp"%>
	<!-- FIM MENU -->
	<div id="content">
		<h1>StringBuilder</h1>
		<br />
		<p>	Ao concatenar uma String geramos uma nova String em memória com o
			resultado da concatenação. <br /> No exemplo abaixo utilizando o
			operador ‘+’ para concatenar, geramos 100 Strings em memória:
		</p>
			<br/>
			<textarea rows="20" cols="70">
			String resultado = "";  
			for (int caractere = 1; caractere <= 100; caractere++) {
			   resultado += caractere;
			   }
			System.out.println(resultado);
			</textarea>
			<br /> 
			No exemplo abaixo utilizando o StringBuilder para concatenar,
			geramos apenas 1 String em memória:
			<br/>
			<textarea rows="30" cols="60">	
			     StringBuilder sb = new StringBuilder();  
			     for (int caractere = 0; caractere <= 100; caractere++) {   
			     sb.append(caractere);  }  System.out.println(sb.toString());
			</textarea>
		<br />
		<br />
		exemplo:
		<br/>
		<textarea rows="20" cols="70">
		StringBuilder sb = new StringBuilder();
			sb.append("Bruno");
			
			out.println(sb.toString());
		</textarea>
		<br />
		<%
			StringBuilder sb = new StringBuilder();
			sb.append("Bruno");

			out.println(sb.toString());
		%>
	</div>
	</main>

</body>
</html>
