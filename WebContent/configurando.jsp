<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<title>Variaveis de Ambiente::</title>
</head>
<body>
	<!-- HEADER -->
	<%@include file="./inc/header.jsp"%>
	<!-- FIM HEADER -->
	<main> 
	<!-- MENU -->
	<%@include file="./inc/menu.jsp"%>
	<!-- FIM MENU -->
	<div id="content">
		<h1>Configurando as vari�veis de ambiente::</h1>
		<br/>
		<p>Para configurar as vari�veis de ambiente � necess�rio ir em "Painel de Controle>>Sistema e Seguran�a>>Sistema"
		e acessar "Configura��es avan�adas do sistema>>Avan�ado>>Vari�veis de ambiente".
		<br/>
		<p>
		Na sess�o "Variaveis do sistema" clique em "novo" no nome da vari�vel coloque "JAVA_HOME" e no valor coloque
		a raiz da instala��o do java, por exemplo "C:\Program files\Java\jdk1.7.0_29".
 		</p>
		<br/>
		<p>
		Na sess�o "vari�veis do sistema" encontre a vari�vel "Path" e clique em "editar", e adicione no final 
		do valor o seguinte texto ";%JAVA_HOME%\bin".
		</p>
		<br/>
		<p>
		Porque devemos configurar as vari�veis do ambiente? a vari�vel JAVA_HOME � importante para que outros programas
		que utilizam o java possam encontrar a JVM e as bibliotecas do java. J� a vari�vel Path � importante para
		que possamos compilar e executar os programas java no prompt de comando.
		</p>
		<br/>
	</div>
	</main>
</body>
</html>