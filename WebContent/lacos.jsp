<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<title>Treinamento La�os de Repeti��o::</title>
</head>
<body>
	<!-- HEADER -->
	<%@include file="./inc/header.jsp"%>
	<!-- FIM HEADER -->
	<main> 
	<!-- MENU -->
	<%@include file="./inc/menu.jsp"%>
	<!-- FIM MENU -->
	<div id="content">
		<h1>La�os, ciclos</h1>
		<br/>
		<h5>FOR</h5>
		<textarea rows="20" cols="70">int total = 0;
		for(int contador = 0; int <= 10; contador++){
		total += contador;
		}</textarea>
		<br/>
		
		<h5>CONTINUE</h5>
		<p>Se houver a necessidade de interromper uma itera��o no la�o podemos utilizar o comando <b>continue;</b></p>
		<br/>
		Ex:
		<br/>
		<textarea rows="30" cols="70">
		for(int i = 0; i <= 10; i++){
			System.out.println("Antes do continue");
			if(i == 4){
				continue;
			}
			System.out.println("Depois do continue" + i);
		}
		</textarea>
		<br/>
		<p>
			No exemplo acima, a primeira e segunda itera��o s�o executadas do inicio ao fim na quarta itera��o executamos
		o comando continue, assim interrompendo a itera��o antes de imprimir a segunda mensagem.
		</p>
		<br/>
		<h5>BREAK</h5>
		<br/>
		<p>
			Se houver necessidade de parar totalmente o for usamos o comando <b>break</b>.
		</p>
		<br/>
			<textarea rows="30" cols="70">
			for(int i = 0; i <= 10; i++){
				System.out.println("Antes do break");
				if(i == 3){
					break;
				}
			}
			</textarea>	
			<br/>
			<br/>
			<h5>FOR EACH</h5>
			<br/>
			<textarea rows="30" cols="70">
			String[]alunos = new String[]{"Bruno", "Irineu","Douglas"};
			for(String nomeAluno : alunos){
				System.out.println("Nome: " + nomeAluno);
			}
			</textarea>
			<br/>
			<h5>While</h5>
			<h5>Do While</h5>
	</div>
	</main>
</body>
</html>
