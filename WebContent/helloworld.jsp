<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<title>HelloWorld::</title>
</head>
<body>
	<!-- HEADER -->
	<%@include file="./inc/header.jsp"%>
	<!-- FIM HEADER -->
	<main> 
	<!-- MENU -->
	<%@include file="./inc/menu.jsp"%>
	<!-- FIM MENU -->
	<div id="content">
		<h1>HelloWorld! Imprimindo na tela</h1>
		<br/>
		<%
		out.print("Hello World");
		%>
		<br/>
		<br/>
		public class HelloWorld{
		<br/>
			public static void main(String[] args){
			<br/>
				System.out.print("HelloWorld");
			<br/>
				//out.print("texto");
			<br/>
			}
			<br/>
		}
		<br/>
		<br/>
		
	</div>
	</main>

</body>
</html>